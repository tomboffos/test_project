<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $medias = [];
        foreach ($this->getMedia('users') as $media){
            $medias[] = $media->getFullUrl();
        }
        // get all media urls
        return [
            'id' => $this->id,
            'headline' => $this->headline,
            'first_name' => $this->first_name,
            'media' => $medias
        ];
    }
}
