<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $medias = [];
        foreach ($this->getMedia('articles') as $media){
            $medias[] = $media->getFullUrl();
        }
        //get all media urls
        return [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'media' => $medias
        ];
    }
}
