<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use App\Models\ProjectArticle;
use App\Models\ProjectHasContent;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    //
    public function create(Project $project,Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);
        // validation
        $data = $request->only('title','content');
        // getting data from request
        $article = ProjectArticle::create($data);
        // orm create projectArticle
        if ($request->has('media')){
            $article->addMediaFromRequest('media')->toMediaCollection('articles');
            // Checking if media request has a file
            // after saving file into the server
        }

        $project->articles()->save($article);
        // After creating project_has_content row or polymorphic table
        return new ProjectResource($project);

    }
}
