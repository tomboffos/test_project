<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use App\Models\ProjectHasContent;
use App\Models\ProjectUser;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //

    public function create(Project $project,Request $request)
    {
        $request->validate([
            'headline' => 'required',
            'first_name' => 'required'
        ]);

        // validation

        $data = $request->only('headline','first_name');

        // getting data from request

        $user = ProjectUser::create($data);

        // creating user by orm
        if ($request->has('media')){
            $user->addMediaFromRequest('media')->toMediaCollection('users');
            // Checking if media request has a file
            // after saving file into the server
        }

        $project->users()->save($user);
        // After creating project_has_content row or polymorphic table

        return new ProjectResource($project);
    }

}
