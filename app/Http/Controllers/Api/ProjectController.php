<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectController extends Controller
{
    //
    public function create(Request $request)
    {
        $request->validate([
            'title'=> 'required',
            'description'=>'required'
        ]);
        // validation check
        $data = $request->only(['title','description']);
        //create data by orm
        return new ProjectResource(Project::create($data));


    }

    public function show(Project $project)
    {
        $articles = $project->articles;
        return  new ProjectResource($project);

    }
}
