<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectHasContent extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = [
        'project_id',
        'project_has_content_id',
        'project_has_content_type'
    ];
}
