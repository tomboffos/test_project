<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = ['title','description'];

    public function articles()
    {
        return $this->morphedByMany(ProjectArticle::class,'project_has_content');
    }

    public function users()
    {
        return $this->morphedByMany(ProjectUser::class,'project_has_content');
    }

    public function project_content()
    {
        return $this->morphTo();
    }
}
