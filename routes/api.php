<?php

use App\Http\Controllers\Api\Project\ArticleController;
use App\Http\Controllers\Api\Project\UserController;
use App\Http\Controllers\Api\ProjectController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('project')->group(function(){
    // Api endpoint to show one project with polymorph entities, {project} - Project id
    Route::get('/{project}',[ProjectController::class,'show']);
    // Endpoint to create one project
    // Request must contain title,description
    Route::post('/',[ProjectController::class,'create']);

    Route::prefix('article')->group(function(){
        // Route to create one article. {project} - Project Id
        // Request must contain title , content optionally media with file
        Route::post('/{project}',[ArticleController::class,'create']);
    });

    Route::prefix('user')->group(function(){
        // Route to create one user. {project} - Project Id
        // request must contain headline , first_name , optionally media with file
        Route::post('/{project}',[UserController::class,'create']);
    });
});
